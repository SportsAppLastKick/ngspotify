import { Component, OnInit } from '@angular/core';
import { ScopesBuilder, AuthConfig, TokenService } from 'spotify-auth';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { InfoService } from './services/info.service';
import { AuthService } from './services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ng-spotify';

  constructor(
    private infoSvc: InfoService,
    private authService: AuthService,
    private router: Router
  ) {
    window.addEventListener(
      'message',
      event => {
        console.log('got postmessage', event);
        const hash = this.IsJsonString(event.data)
          ? JSON.parse(event.data)
          : event.data;
        if (hash.type === 'access_token') {
          this.authService.setAccessToken(
            hash.access_token,
            hash.expires_in || 60
          );

        this.router.navigate(['account']);
          this.checkUser(true);
        }
      },
      false
    );
  }

  private IsJsonString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  checkUser(redirectToLogin) {
    this.infoSvc.fetchUserInfo().subscribe(
      userInfo => {
        console.log(userInfo);
        this.authService.setUsername(userInfo.display_name);
        this.authService.setUserCountry(userInfo.country);

        const avatarUrl = userInfo.images &&  userInfo.images.length ?
        userInfo.images[0].url : 'assets/user.png';
        this.authService.setAvatar(avatarUrl);

        this.authService.notifyChangeAuthentication(true);
      }
    );
  }

  ngOnInit(): void {
    // this.authService.authorizedStream.subscribe(() => {
    //   this.router.navigate(['user']);
    // });
  }
}
