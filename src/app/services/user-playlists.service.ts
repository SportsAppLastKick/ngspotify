import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserPlaylist } from 'src/app/model/user-playlist.model';

@Injectable({
  providedIn: 'root'
})
export class UserPlaylistsService {

  constructor(private httpClient: HttpClient) { }

  getUsrPlaylists(): Observable<UserPlaylist> {
    return this.httpClient.get<UserPlaylist>(environment.baseEndpoint + 'me/playlists');
  }
}


