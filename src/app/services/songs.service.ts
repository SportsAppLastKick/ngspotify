import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FavouriteSongsModel } from '../model/my-songs.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SongsService {

  constructor(private httpClient: HttpClient) { }

  getFavouriteSongs(): Observable<FavouriteSongsModel> {
    return this.httpClient.get<FavouriteSongsModel>(environment.baseEndpoint + 'me/tracks');
  }

  checkIfSongsIsSaved(ids: string[]) {

    const commaIds = ids.join(',');

    const params = new HttpParams().set('ids', commaIds);
    return this.httpClient.get(environment.baseEndpoint + 'me/tracks/contains', {
      params: params
    });
  }
}
