import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';
/*import { NewReleaseModel } from '../model/new-release.model';*/
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {
  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {}

  getMyTracks() {
    // const token = this.authService.getAccessToken()
    return this.httpClient.get(environment.baseEndpoint + '/me/tracks');
  }
  getAlbums() {
    // const token = this.authService.getAccessToken()
    return this.httpClient.get(environment.baseEndpoint + '/me/albums');
  }

  getCategories() {
    return this.httpClient.get(environment.baseEndpoint + '/browse/categories');
  }

  getFeaturedPlaylist() {
    return this.httpClient.get(
      environment.baseEndpoint + '/browse/featured-playlists'
    );
  }

  // getNewRelease(): Observable<NewReleaseModel> {
  //   return this.httpClient.get<NewReleaseModel>(
  //     environment.baseEndpoint + '/browse/new-releases'
  //   );
  // }

  getReccomandationsGenreSeeds() {
    return this.httpClient.get(
      environment.baseEndpoint + '/recommendations/available-genre-seeds'
    );
  }

  getTopItems(type: 'tracks' | 'artists') {
    return this.httpClient.get(environment.baseEndpoint + '/me/top/' + type);
  }

  getPlaylists() {
    return this.httpClient.get(environment.baseEndpoint + '/me/playlists');
  }

  getPlaylist(playlistId: string) {
    return this.httpClient.get(
      environment.baseEndpoint + '/playlists/' + playlistId
    );
  }
}
