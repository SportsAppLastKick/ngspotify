import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { FeaturedPlaylist } from '../model/featured-playlist.model';


import { NewReleaseModel } from '../model/new-release.model';


@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private httpClient: HttpClient) { }


  getFeaturedPlaylist(): Observable<FeaturedPlaylist> {
    return this.httpClient.get<FeaturedPlaylist>(
      environment.baseEndpoint + '/browse/featured-playlists'
    );
  }

  getNewRelease(): Observable<NewReleaseModel> {
    return this.httpClient.get<NewReleaseModel>(
      environment.baseEndpoint + '/browse/new-releases'
    );
  }
}
