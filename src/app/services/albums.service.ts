import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AlbumListModel } from 'src/app/model/my-albums.model';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  constructor(private httpClient: HttpClient) { }

  getAlbums(): Observable<AlbumListModel>{
      return this.httpClient.get<AlbumListModel>(environment.baseEndpoint + 'me/albums');
  }
}

