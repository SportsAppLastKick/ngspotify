import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PlaylistDetailModel } from 'src/app/model/playlist-detail.model';
import { PlaylistsModel } from '../model/playlists.model';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsDetailService {

  constructor(private http: HttpClient) { }
  getCurrentPlaylist(): Observable<PlaylistsModel> {
    return this.http.get<PlaylistsModel>(environment.baseEndpoint + 'playlists/{playlist_id}/tracks');
  }

  getPlaylistDetail(id: string): Observable<PlaylistDetailModel> {
    return this.http.get<PlaylistDetailModel>(environment.baseEndpoint + 'playlists/' + id);
  }
}
