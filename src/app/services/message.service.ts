import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { AppMessage } from '../model/app-message.model';
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  eventBus = new Subject<AppMessage>();

  constructor() { }

  broadcast(key, data?) {
    this.eventBus.next({ key, data });
  }

  on(key: string) : Observable<any> {
   return this.eventBus.asObservable().pipe(
      filter(c => c.key === key),
      map(ev => ev.data)
    )
  }
}
