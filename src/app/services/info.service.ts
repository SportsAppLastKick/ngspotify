import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Router } from '@angular/router';
import { tap, catchError } from 'rxjs/operators';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { UserModel } from '../model/user.model';
import { environment } from 'src/environments/environment';
import { FollowingArtistsModel } from '../model/following-artists.model';

@Injectable()
export class InfoService {
  private user: {} = {};
  private user$: BehaviorSubject<{}>;

  constructor(private http: HttpClient, private router: Router) {
    this.user$ = new BehaviorSubject<{}>(this.user);
  }

  public fetchUserInfo(): Observable<UserModel> {
    return this.http.get<UserModel>(
      environment.baseEndpoint + environment.apiUrls.account.me
    );
  }

  public fetchUserInfoDetail(userInfoApiUrl: string): Observable<{}> {
    return this.http.get(userInfoApiUrl);
  }

  public getFollowingArtists(): Observable<FollowingArtistsModel> {
    return this.http.get<FollowingArtistsModel>(
      environment.baseEndpoint + environment.apiUrls.account.followingArtists
    );
  }

  setFollowArtist(type: 'artist' | 'user', ids?: string[]) {
    const params = new HttpParams().set('type', type);

    return this.http.put(
      environment.baseEndpoint + 'me/following',
      {
        ids
      },
      {
        params
      }
    );
  }

  removeFollowArtist(type: 'artist' | 'user', ids?: string[]) {
    // const params = new HttpParams()
    // .set('type', type);

    return this.http.delete(
      environment.baseEndpoint + 'me/following/?type=' + type + '&ids=' + ids[0]
    );
  }

  public getUserStream(): Observable<{}> {
    return this.user$.asObservable();
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      (result as any) = error;
      return of(result as T);
    };
  }
}
