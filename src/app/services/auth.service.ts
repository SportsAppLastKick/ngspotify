import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import * as moment from 'moment';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private httpClient: HttpClient,
    private messageService : MessageService) {}

  getLoginURL(scopes) {
    return (
      'https://accounts.spotify.com/authorize?client_id=' +
      environment.clientId +
      '&redirect_uri=' +
      encodeURIComponent(environment.redirectUrl) +
      '&scope=' +
      encodeURIComponent(scopes.join(' ')) +
      '&response_type=token'
    );
  }

  notifyChangeAuthentication(isAuth){
    this.messageService.broadcast(environment.subjectKeys.changeAuth, isAuth);
  }

  setAccessToken(token, expiresIn) {
    // console.log(expiresIn);

    const momentExpirationMilliseconds = moment().add(Number(expiresIn), 'seconds').valueOf();
    // const momentNow = moment();
    // const diff = momentExp.diff(momentNow,'seconds');
    // console.log(momentExpirationMilliseconds);
    // console.log(momentNow);

    // console.log(diff);

    localStorage.setItem('pa_token', token);
    localStorage.setItem(
      'pa_expires', String(momentExpirationMilliseconds));
  }
  getAccessToken() {
    const expires = 0 + localStorage.getItem('pa_expires');

    const momentNow = moment().valueOf();
    const expirationTime = Number(expires);

    console.log(momentNow);
    console.log(expirationTime);

    console.log(momentNow - expirationTime);

    if (momentNow > expirationTime) {
      return '';
    }
    const token = localStorage.getItem('pa_token');
    return token;
  }
  getMe() {
    // const headers = new HttpHeaders();
    // headers.append('Authorization', 'Bearer ' + this.getAccessToken());

    return this.httpClient.get(environment.redirectUrl + '/me');
  }

  getUsername() {
    const username = localStorage.getItem('pa_username');
    return username;
  }
  setUsername(username) {
    localStorage.setItem('pa_username', username);
  }
  getUserCountry() {
    const userCountry = localStorage.getItem('pa_usercountry');
    return userCountry;
  }
  setUserCountry(userCountry) {
    localStorage.setItem('pa_usercountry', userCountry);
  }
  getAvatar() {
    const avatar = localStorage.getItem('pa_avatar');
    return avatar;
  }
  setAvatar(avatar) {
    localStorage.setItem('pa_avatar', avatar);
  }
}
