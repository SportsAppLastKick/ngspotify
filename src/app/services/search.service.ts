import { Injectable } from '@angular/core';
import { NgSpotifyPage } from '../model/page.enum';
import { MessageService } from './message.service';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private messageService: MessageService) { }

  onSearch(searchValue, page: NgSpotifyPage,key) {
    this.messageService.broadcast(key, searchValue);
  }
}
