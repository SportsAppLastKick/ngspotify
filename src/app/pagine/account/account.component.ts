import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { InfoService } from 'src/app/services/info.service';
import { concatMap, map, toArray } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import {
  FollowingArtistsModel,
  FollowingArtistsItem,
  FollowingArtistsImage
} from 'src/app/model/following-artists.model';
import { UserPlaylistsService } from 'src/app/services/user-playlists.service';
import { UserPlaylist, UserPlaylistItem } from 'src/app/model/user-playlist.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  @ViewChild('box') box: ElementRef;

  followingArtists: FollowingArtistsItem[] = [];
  userPlaylists: UserPlaylistItem[] = [];

  active: number;

  constructor(
    private fetchUser: InfoService,
    private authService: AuthService,
    private playlistService: UserPlaylistsService,
    private router: Router
  ) {}

  ngOnInit() {
    this.setActiveTab(1);
  }

  get username() {
    return this.authService.getUsername();
  }
  get avatar() {
    return this.authService.getAvatar();
  }

  setActiveTab(tabId) {
    this.active = tabId;

    switch (this.active) {
      case 1:
        this.getPlaylists();
        break;
      case 2:
        this.getArtists();
        break;
      default:
        break;
    }
  }

  getArtists() {
    // if (this.followingArtists && this.followingArtists.length) {
    //   return;
    // }
    this.fetchUser.getFollowingArtists().subscribe(res => {
      if (res && res.artists && res.artists.items) {
        this.followingArtists = res.artists.items;
        console.log(this.followingArtists);
      }
    });
  }

  getPlaylists() {
    this.playlistService.getUsrPlaylists().subscribe(res => {
      if (res && res.items) {
        this.userPlaylists = res.items;
        console.log(this.userPlaylists);
      }
    });
  }

  toggleFollowing(artist: FollowingArtistsItem) {
    if (artist) {
      this.fetchUser.setFollowArtist('artist', [artist.id]).subscribe(res => {
        console.log(res);
        artist.isUnfollowed = false;
      });
    }
  }

  removeFollowing(artist: FollowingArtistsItem) {
    if (artist) {
      this.fetchUser
        .removeFollowArtist('artist', [artist.id])
        .subscribe(res => {
          console.log(res);
          artist.isUnfollowed = true;
        });
    }
  }

  scrollShow() {
    const scroll = document.getElementById('box').scrollTop;
    if ( scroll > 30) {
      // this.box.nativeElement.children[0].classList.remove('user-container');
      // this.box.nativeElement.children[0].classList.add('user-container-small');
      // this.box.nativeElement.children[0].classList.remove('avatar');
      // this.box.nativeElement.children[0].classList.add('avatar-small');
      // this.box.nativeElement.children[0].classList.remove('user-title');
      // this.box.nativeElement.children[0].classList.add('user-title-small');
      // this.box.nativeElement.children[0].classList.remove('user');
      // this.box.nativeElement.children[0].classList.add('user-small');
      console.log('scroll');
    }

  }

  titleClick(id) {
    this.router.navigate([`playlist/${id}`]);
  }

  /* esempio concatMap
  getUser() {
    this.fetchUser
      .fetchUserInfo()
      .pipe(
        concatMap(user => {
          return this.fetchUser.getFollowingArtists();
        })
      )
      .subscribe(res => console.log(res));
  } */
}
