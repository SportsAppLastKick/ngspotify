import { Component, OnInit, Input } from '@angular/core';
import { PlaylistsDetailService } from 'src/app/services/playlists-detail.service';
import { PlaylistDetailModel } from 'src/app/model/playlist-detail.model';
import { Image } from 'src/app/model/playlist-detail.model';
import { ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { concatMap } from 'rxjs/operators';
import { NgSpotifyPage } from 'src/app/model/page.enum';
import { MessageService } from 'src/app/services/message.service';
import { environment } from 'src/environments/environment';
import { SongsService } from 'src/app/services/songs.service';

@Component({
  selector: 'app-playlist-detail',
  templateUrl: './playlist-detail.component.html',
  styleUrls: ['./playlist-detail.component.css']
})
export class PlaylistDetailComponent implements OnInit {

  playlistDet: PlaylistDetailModel;

  currentPage = NgSpotifyPage.MyPlaylistDetail;
  searchText: string;
  searchProperty = 'track.name';
  keySubject = environment.subjectKeys.myPlaylistDetail;
  


  constructor(private playService: PlaylistsDetailService,
    private ar: ActivatedRoute,
    private songsService: SongsService,
    private messageService: MessageService) {

    this.messageService.on(environment.subjectKeys.myPlaylistDetail).subscribe(res => {
      this.searchText = res;
    })

    this.ar.params.pipe(
      concatMap(route => this.playService.getPlaylistDetail(route['id']))
    )
      .subscribe(res => {

        this.playlistDet = res;

        const tracksIDs = this.playlistDet && this.playlistDet.tracks && this.playlistDet.tracks.items ? this.playlistDet.tracks.items.map(i => i.track.id)
          : [];


        this.songsService.checkIfSongsIsSaved(tracksIDs.filter((val,index)=>{
          return index <50
        })).subscribe(resCheck => {
          console.log(resCheck);
          this.playlistDet.tracks.items = this.playlistDet.tracks.items.map((value, index) => {
            const newItem = {
              ...value,
              isFavourite: resCheck[index]
            }
            return newItem;
          })
          console.log(this.playlistDet);


        })
      })
  }

  ngOnInit() {

    
    // this.playService.getCurrentPlaylist()
    // .subscribe(res=>{
    //   if(res&&res.items){
    //     this.playlistDet=res.items
    //   }
    //   console.log(res);
    // });

  }

  getPlaylistImage(images: Image[]) {
    if (images && images.length) {
      const image = images.find(i => i.height <= 300 || i.height >= 240);
      return image ? image.url : ''
    } else {
      return '';
    }
  }

  getPlaylistDuration(){
    
  }

}
