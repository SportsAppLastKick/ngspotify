import { Component, OnInit } from '@angular/core';
import { AlbumsService } from 'src/app/services/albums.service';
import { Item, Image } from 'src/app/model/my-albums.model';
import * as _ from 'lodash';
import { MessageService } from 'src/app/services/message.service';
import { environment } from 'src/environments/environment';
import { NgSpotifyPage } from 'src/app/model/page.enum';
import { SearchService } from 'src/app/services/search.service';
@Component({
  selector: 'app-album-page',
  templateUrl: './album-page.component.html',
  styleUrls: ['./album-page.component.css']
})
export class AlbumPageComponent implements OnInit {

  sortFilters = [
    'Artista',
    'Titolo',
    'Aggiunti di recente'
  ];
  currentPage = NgSpotifyPage.MyAlbums;
  selectedFilter: string = 'Artista';
  albums: Item[];

  searchText: string;
  searchProperty = 'album.name';
  keySubject = environment.subjectKeys.myAlbums;

  constructor(private albumsService: AlbumsService,
    private searchService: SearchService,
    private messageService: MessageService) {
    this.messageService.on(environment.subjectKeys.myAlbums).subscribe((res: string) => {
      console.log(res);
      this.searchText = res;
    })
  }

  ngOnInit() {
    this.albumsService.getAlbums()
      .subscribe(res => {
        if (res && res.items) {
          this.albums = res.items;
        } console.log(res)
      });

  }
  changeSortFilter(selectedFilter) {
    this.selectedFilter = selectedFilter;

    switch (this.selectedFilter) {
      case 'Artista':
        this.albums = _.orderBy(this.albums, ['album.artists[0].name'], ['asc']);
        break;
      case 'Titolo':
        this.albums = _.orderBy(this.albums, ['album.name'], ['asc']);
        break;
      case 'Aggiunti di recente':
        this.albums = _.orderBy(this.albums, ['added_at'], ['asc']);
        break;
      default:
        break;
    }
  }

  getBackgroundImage(images: Image[]) {
    if (images && images.length) {
      const image = images.find(i => i.height <= 300 || i.height >= 240);
      return image ? image.url : ''
    } else {
      return '';
    }
  }
}
