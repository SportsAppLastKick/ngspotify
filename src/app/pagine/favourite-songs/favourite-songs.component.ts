import { Component, OnInit } from '@angular/core';
import { SongsService } from 'src/app/services/songs.service';
import { Item } from 'src/app/model/my-songs.model';
import { NgSpotifyPage } from 'src/app/model/page.enum';
import { MessageService } from 'src/app/services/message.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-favourite-songs',
  templateUrl: './favourite-songs.component.html',
  styleUrls: ['./favourite-songs.component.css']
})
export class FavouriteSongsComponent implements OnInit {

  songs: Item[];
  currentPage = NgSpotifyPage.MySongs;
  searchText: string;
  searchProperty = 'track.name';  
  keySubject = environment.subjectKeys.mySongs;

  constructor(private songsService: SongsService,
    private messageService: MessageService) {
    this.messageService.on(environment.subjectKeys.mySongs).subscribe(res => {
      console.log(res);
      this.searchText = res;
    })
  }

  ngOnInit() {
    this.songsService.getFavouriteSongs()
      .subscribe(res => {
        if (res && res.items) {
          this.songs = res.items;
        }
        console.log(res)
      });
  }



}