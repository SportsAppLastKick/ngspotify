import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('cardsView') cardsViewRef: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  /* FUNZIONALITÀ PER LO SLIDE DELLE IMMAGINI */
  onSlideLeft() {
    this.onSlide(-1);
    /* console.log(this.cardsViewRef.nativeElement.children); */
  }

  onSlideRight() {
    this.onSlide(1);
    /* console.log(this.cardsViewRef.nativeElement.children); */
  }

  onSlide(num: number): void {
    const marginLeft = this.cardsViewRef.nativeElement.children[0].style.marginLeft;
    let posizione = Number(marginLeft.slice(0, marginLeft.length - 2));
    const larghezza = this.cardsViewRef.nativeElement.children[0].offsetWidth;
    const scroll = .8 * this.cardsViewRef.nativeElement.clientWidth;
    if (num === 1) {
      /* console.log('condizione1: ' + (scroll >= (larghezza + posizione) )); */
      (scroll >= (larghezza + posizione) ) ? posizione = -(larghezza - scroll + 2) : posizione -= scroll;
    } else {
      /* console.log('condizione2: ' + ( -posizione - scroll < 0 ) ); */
      ( -posizione - scroll < 0 ) ? posizione = 0 : posizione += scroll;
    }
    /* console.log(' posizione: ' + posizione + '; scroll: ' + scroll); */
    this.cardsViewRef.nativeElement.children[0].style.marginLeft = posizione + 'px';
  }

}
