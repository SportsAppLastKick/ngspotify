import { UserComponent } from './components/user/user.component';
import { SpotifyAuthModule } from 'spotify-auth';
import { LoginComponent } from './components/login/login.component';
import { AlbumPageComponent } from './pagine/album-page/album-page.component';
import { FavouriteSongsComponent } from './pagine/favourite-songs/favourite-songs.component';
import { ProfileComponent } from './shared/profile/profile.component';
import { AccountComponent } from './pagine/account/account.component';
import { HomeComponent } from './pagine/home/home.component';
import { PlaylistDetailComponent} from './pagine/playlist-detail/playlist-detail.component';

export const routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'user',
    component: UserComponent
  },
  {
    path: 'album',
    component: AlbumPageComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'fav',
    component: FavouriteSongsComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'account',
    component: AccountComponent
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'playlist/:id',
    component: PlaylistDetailComponent,
  },
  SpotifyAuthModule.authRoutes()[0]
];
