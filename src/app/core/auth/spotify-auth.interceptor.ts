import { Injectable } from '@angular/core';
import { TokenService, SpotifyAuthInterceptor } from 'spotify-auth';
import 'rxjs/add/operator/do';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent
} from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class NgSpotifyAuthInterceptor implements HttpInterceptor {
  // doOnError(err: any): void {}

  constructor(private authService: AuthService) {
    // super(tokenSvc);
  }

  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const authReq = req.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.authService.getAccessToken()
      }
    });
    return next.handle(authReq);
  }
}
