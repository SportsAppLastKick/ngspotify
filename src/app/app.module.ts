import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SpotifyAuthModule } from 'spotify-auth';
import { RouterModule } from '@angular/router';
import { routes } from './app.routes';
import { InfoService } from './services/info.service';
import { NgSpotifyAuthInterceptor } from './core/auth/spotify-auth.interceptor';
import { UserComponent } from './components/user/user.component';
import { PlayerComponent } from './components/player/player.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { HeartComponent } from './shared/heart/heart.component';
import { SearchbarComponent } from './shared/searchbar/searchbar.component';
import { ArrowsComponent } from './shared/arrows/arrows.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AlbumPageComponent } from './pagine/album-page/album-page.component';
import { FavouriteSongsComponent } from './pagine/favourite-songs/favourite-songs.component';
import { PlaylistDetailComponent } from './pagine/playlist-detail/playlist-detail.component';
import { ProfileComponent } from './shared/profile/profile.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { AccountComponent } from './pagine/account/account.component';
import { HomeComponent } from './pagine/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandlerInterceptor } from './core/error-handler/error-handler.interceptor';
import { CardsSliderComponent } from './components/cards-slider/cards-slider.component';
import { CardComponent } from './components/cards-slider/card/card.component';
import { registerLocaleData } from '@angular/common';
import { AlbumsService } from './services/albums.service';
import { SongsService } from './services/songs.service';
import { FollowUnfollowComponent } from './components/follow-unfollow/follow-unfollow.component';
import { UserPlaylistsService } from './services/user-playlists.service';
import { PageSearchBarComponent } from './shared/page-search-bar/page-search-bar.component';
import { SearchPipe } from './shared/pipes/search.pipe';
import { FollowingComponent } from './components/following/following.component';
import { PublicPlaylistComponent } from './components/public-playlist/public-playlist.component';
import { TrackListComponent } from './shared/track-list/track-list.component';
import { NgxsModule } from '@ngxs/store';

// registerLocaleData(localeIt, 'it');
@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [FollowUnfollowComponent, AppComponent, LoginComponent, UserComponent, PlayerComponent, NavbarComponent, HeartComponent, SearchbarComponent, ArrowsComponent, SidebarComponent, AlbumPageComponent, FavouriteSongsComponent, ProfileComponent, DropdownComponent, AccountComponent, HomeComponent, CardsSliderComponent, CardComponent, PlaylistDetailComponent, PageSearchBarComponent, SearchPipe, FollowingComponent, PublicPlaylistComponent,TrackListComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    SpotifyAuthModule.forRoot(),
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    NgxsModule.forRoot([]),
  ],
  providers: [
    InfoService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NgSpotifyAuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    },

    SongsService, AlbumsService, UserPlaylistsService, /* PlaylistDetailService */
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
