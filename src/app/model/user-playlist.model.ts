export interface ExternalUrls {
    spotify: string;
}

export interface ExternalUrls2 {
    spotify: string;
}

export interface Owner {
    external_urls: ExternalUrls2;
    href: string;
    id: string;
    type: string;
    uri: string;
}

export interface Tracks {
    href: string;
    total: number;
}

export interface UserPlaylistItem {
    collaborative: boolean;
    external_urls: ExternalUrls;
    href: string;
    id: string;
    images: any[];
    name: string;
    owner: Owner;
    public: boolean;
    snapshot_id: string;
    tracks: Tracks;
    type: string;
    uri: string;
}

export interface UserPlaylist {
    href: string;
    items: UserPlaylistItem[];
    limit: number;
    next?: any;
    offset: number;
    previous?: any;
    total: number;
}
