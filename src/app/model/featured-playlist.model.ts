export interface FeaturedPlaylist {

  message: string;
  playlists: Playlists;
}

export interface Playlists {
  href: string;
  items: Item[];
  limit: number;
  next: string;
  offset: number;
  previous?: any;
  total: number;
}

export interface Item {
  collaborative: boolean;
  external_urls: Externalurls;
  href: string;
  id: string;
  images: Image[];
  name: string;
  owner: Owner;
  public?: any;
  snapshot_id: string;
  tracks: Tracks;
  type: string;
  uri: string;
}

export interface Tracks {
  href: string;
  total: number;
}

export interface Owner {
  external_urls: Externalurls;
  href: string;
  id: string;
  type: string;
  uri: string;
}

export interface Image {
  height: number;
  url: string;
  width: number;
}

export interface Externalurls {
  spotify: string;
}
