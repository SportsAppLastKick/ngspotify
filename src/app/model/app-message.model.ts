export class AppMessage {
    key: string;
    data?: any;

    constructor(key, data) {
        this.data = data;
        this.key = key;
    }
}