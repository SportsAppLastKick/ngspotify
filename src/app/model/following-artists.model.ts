export interface FollowingArtistsModel {
  artists: Artists;
}

export interface Artists {
  items: FollowingArtistsItem[];
  next?: any;
  total: number;
  cursors: Cursors;
  limit: number;
  href: string;
}

interface Cursors {
  after?: any;
}

export interface FollowingArtistsItem {
  external_urls: Externalurls;
  followers: Followers;
  genres: string[];
  href: string;
  id: string;
  images: FollowingArtistsImage[];
  name: string;
  popularity: number;
  type: string;
  uri: string;
  isUnfollowed?: boolean;
}

export interface FollowingArtistsImage {
  height: number;
  url: string;
  width: number;
}

interface Followers {
  href?: any;
  total: number;
}

interface Externalurls {
  spotify: string;
}
