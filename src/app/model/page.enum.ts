export enum NgSpotifyPage {
    MyAlbums,
    MySongs,
    MyPlaylistDetail
}