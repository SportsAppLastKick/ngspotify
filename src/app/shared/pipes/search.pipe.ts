import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';
@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(items: any[], property: string, searchText: string): any[] {

    if (!items) return [];
    if (!searchText) return items;
    searchText = searchText.toLowerCase();
    if (property) {
      return items.filter(item => {
        if (_.get(item, property) === undefined) {
          return items;
        }
        return _.get(item, property).toLowerCase().includes(searchText);
      });
    } else {
      return items.filter(item => {
        return item.toLowerCase().includes(searchText);
      });
    }
  }

}
