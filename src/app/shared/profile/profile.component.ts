import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  showDropdown = false;

  constructor(private authService: AuthService) {}

  ngOnInit() {}

  toggleDropdown() {
    this.showDropdown = !this.showDropdown;
  }

  get username() {
    return this.authService.getUsername();
  }

  get avatar() {
    return this.authService.getAvatar();
  }
}
