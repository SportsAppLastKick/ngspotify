import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-track-list',
  templateUrl: './track-list.component.html',
  styleUrls: ['./track-list.component.css'],
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class TrackListComponent {

 @Input() tracksList : Array<any>;
 @Input() searchText : string;
 @Input() searchProperty : string;
 @Input() isFavouriteSongs : boolean;
  constructor() { }


}
