import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SearchService } from 'src/app/services/search.service';
import { NgSpotifyPage } from 'src/app/model/page.enum';

@Component({
  selector: 'app-page-search-bar',
  templateUrl: './page-search-bar.component.html',
  styleUrls: ['./page-search-bar.component.css']
})
export class PageSearchBarComponent {

  searchText: string;
  @Input() currentPage: NgSpotifyPage;
  @Input() keySubject: NgSpotifyPage;
  constructor(private searchService: SearchService) { }

  onDigit(text) {
    console.log(text)
    this.searchText = text;

    this.searchService.onSearch(this.searchText, this.currentPage, this.keySubject);

  }

}
