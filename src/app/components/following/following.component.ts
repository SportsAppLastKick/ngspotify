import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FollowingArtistsItem, FollowingArtistsImage } from 'src/app/model/following-artists.model';

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.css']
})
export class FollowingComponent  {

  @Input() followingArtists: FollowingArtistsItem[] = [];
  @Output() onToggleFollowing = new EventEmitter<any>();
  @Output() onRemoveFollowing = new EventEmitter<any>();

  constructor() { }

  getArtistImage(images: FollowingArtistsImage[]) {
    const artistImage = images.find(
      img => img.width <= 200 || img.width >= 160
    );

    return artistImage ? artistImage.url : '/src/assets/download.jpeg';
  }

  toggleFollowing(artist: FollowingArtistsItem) {
    this.onToggleFollowing.emit(artist);
  }

  removeFollowing(artist: FollowingArtistsItem) {
    this.onRemoveFollowing.emit(artist);
  }

}
