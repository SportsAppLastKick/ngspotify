import { Component, OnInit } from '@angular/core';
import { TokenService, ScopesBuilder, AuthConfig } from 'spotify-auth';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private tokenSvc: TokenService,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.authService.getAccessToken() !== '') {
      this.router.navigate(['home']);
    }
  }

  public login(): void {
    const url = this.authService.getLoginURL([
      'user-read-private',
      'playlist-read-private',
      'playlist-modify-public',
      'playlist-modify-private',
      'user-library-read',
      'user-library-modify',
      'user-follow-read',
      'user-follow-modify'
    ]);

    const width = 450,
      height = 730,
      left = screen.width / 2 - width / 2,
      top = screen.height / 2 - height / 2;

    const w = window.open(
      url,
      'Spotify',
      'menubar=no,location=no,resizable=no,scrollbars=no,status=no,' +
        'width = ' +
        width +
        ', height = ' +
        height +
        ', top = ' +
        top +
        ', left = ' +
        left
    );
  }
}
