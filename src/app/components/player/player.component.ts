import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  started = false;

  constructor() { }

  ngOnInit() {
  }

  onToggle() {
    this.started = !this.started;
  }
}
