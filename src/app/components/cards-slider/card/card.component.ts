import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() title = '';
  @Input() artist = '';
  @Input() followers = '';
  @Input() backgroundImage = '';

  @Output() onTitleClick = new EventEmitter<any>();

  constructor() { }

  titleClick() {
    this.onTitleClick.emit();
  }

}
