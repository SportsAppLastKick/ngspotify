import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-follow-unfollow',
  templateUrl: './follow-unfollow.component.html',
  styleUrls: ['./follow-unfollow.component.css']
})
export class FollowUnfollowComponent {

  @Input() isUnfollowed: boolean;

  @Output() onToggleFollowing = new EventEmitter<any>();
  @Output() onRemoveFollowing = new EventEmitter<any>();

  constructor() {}

  toggleFollowing() {
    this.onToggleFollowing.emit();
  }

  removeFollowing() {
    this.onRemoveFollowing.emit();
  }
}
