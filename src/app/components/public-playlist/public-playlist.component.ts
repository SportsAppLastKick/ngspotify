import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UserPlaylist, UserPlaylistItem } from 'src/app/model/user-playlist.model';

@Component({
  selector: 'app-public-playlist',
  templateUrl: './public-playlist.component.html',
  styleUrls: ['./public-playlist.component.css']
})
export class PublicPlaylistComponent {

  @Input() userPlaylists: UserPlaylistItem[] = [];

  @Output() onTitleClick = new EventEmitter<any>();

  constructor() { }

  titleClick(id: string) {
    this.onTitleClick.emit(id);
  }

}
