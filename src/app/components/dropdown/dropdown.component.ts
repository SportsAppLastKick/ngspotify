import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-dropdown',
  animations:[
    trigger('fade', [
      transition(':enter', [
        style({opacity: 0}),
        animate('5s', style({opacity: 1}))
      ])
    ])
  ],
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
