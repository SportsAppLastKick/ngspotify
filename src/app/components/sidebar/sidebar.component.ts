import { Component, OnInit } from '@angular/core';
import { UserPlaylistsService } from 'src/app/services/user-playlists.service';
import { UserPlaylistItem } from 'src/app/model/user-playlist.model';
import { MessageService } from 'src/app/services/message.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  playlists: UserPlaylistItem[];

  constructor(private playService: UserPlaylistsService,
    private messageService: MessageService) {
    this.messageService.on(environment.subjectKeys.changeAuth).subscribe((res: boolean) => {
      console.log(res);
      if (res) {
        this.getPlaylists();
      }
    })
  }

  ngOnInit() {
    //  this.getPlaylists();
  }


  getPlaylists() {
    this.playService.getUsrPlaylists()
      .subscribe(res => {
        if (res && res.items) {
          this.playlists = res.items;
        }
        console.log(res);
      });
  }

}
