// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  clientId: 'e1abde46cbf942348d99a5e1feececfc',
  redirectUrl: 'http://localhost:4200/callback.html',
  baseEndpoint: 'https://api.spotify.com/v1/',

  apiUrls: {
    account: {
      followingArtists: 'me/following?type=artist',
      me: 'me'
    }
  },
  subjectKeys: {
    myAlbums: 'myAlbumsSearch',
    mySongs: 'mySongs',
    myPlaylistDetail : 'myPlaylistDetail',
    changeAuth : 'changeAuth'

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
